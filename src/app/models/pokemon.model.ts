export interface Pokemons {
    id: number;
    name: string;
    types?: string;

    weight: number;
    height: number;
    abilities: any[];
    base_ex: number;
    moves: [];
    sprites?: Sprites;
    forms?: any;
    stats?: any[];
}

interface Sprites {
    front_default: string;
    back_default: string;
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Pokemons } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  public pokemonCollection: BehaviorSubject<Pokemons[]> = new BehaviorSubject([]);

  constructor() { }

  public addPokemon(pokemon: Pokemons): void {
    const currentCollection = [...this.pokemonCollection.value];
    currentCollection.push(pokemon);
    this.pokemonCollection.next(currentCollection);
    localStorage.setItem("pokemon-collection", JSON.stringify(this.pokemonCollection.value));
   
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Pokemons } from '../../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})

export class PokemonService {

  constructor(private http: HttpClient) { }

  getPokemons() {
    return this.http.get<Pokemons[]>(`${environment.apiBaseUrl}/?limit=151`).toPromise().then((r: any) => r.results);
  }

  getPokemonData(name: string) {
    return this.http.get(`${environment.apiBaseUrl}/${name}`).toPromise()
  }

  savePokemon(pokemon: Pokemons): any {
    localStorage.setItem('pokemon', JSON.stringify(pokemon))
  }

  getCapturedPokemon(): Pokemons {
    const capturedPokemon = JSON.parse(localStorage.getItem('capturedPokemon'))
    return capturedPokemon ? capturedPokemon : null
  }

}
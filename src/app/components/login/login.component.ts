import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required,
                                    Validators.minLength(3),
                                    Validators.maxLength(25)]),
  });

  isLoading: boolean = false;
  loginError: string;

  constructor(private router: Router, private session: SessionService) { }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');
  }

  async onLoginClicked() {
    //this.isLoading = true;
    localStorage.setItem('username', this.username.value);
    console.log(this.username.value);
    this.router.navigateByUrl( `/home` );
    console.log("navigating");
  }
}

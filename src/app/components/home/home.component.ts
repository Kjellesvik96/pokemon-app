import { Component, OnInit } from '@angular/core';
import { Pokemons } from '../../models/pokemon.model';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public pokemons: Pokemons[] = [];

  constructor(private pokemonService: PokemonService, private router: Router) { }

  onPokemonClick(name): void {
    this.router.navigateByUrl( `/info/${ name }` );
    console.log(name);
  }

  async ngOnInit() {
    try {
      this.pokemons = await this.pokemonService.getPokemons();
      console.log(this.pokemons);
    } catch (e) {
      console.error(e.message);
    };
  }
}
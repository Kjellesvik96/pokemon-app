import { Component, OnInit } from '@angular/core';
import { Pokemons } from '../../models/pokemon.model';
import { TrainerService } from '../../services/trainer/trainer.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public pokemonList: Pokemons[] = [];
  public trainer = null;


  constructor(private trainerService: TrainerService,
              private router: Router) { }

  get collectionList$(): Observable<Pokemons[]> {
    return this.trainerService.pokemonCollection;
  }

  ngOnInit(): void {
    this.trainer = localStorage.getItem("trainer");
  }

  goToDetails(id){
    this.router.navigate(['/catalogue', id]);
  }
 
  onLogoutClick(){
    localStorage.clear();
  }
}

  /*onPokemonSelected(pokemon: Pokemons) {
    this.pokemonService.savePokemon(pokemon)
    this.router.navigateByUrl(`/pokemon/${pokemon.name}`)
  }

  onRemoveClick() {
    localStorage.removeItem(this.pokemon)
  }*/
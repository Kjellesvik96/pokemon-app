import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  public name: string;
  pokemon;

  constructor(private route: ActivatedRoute, private pokemonService: PokemonService) {
    this.name = this.route.snapshot.paramMap.get('name')
   }

  async ngOnInit(){
    try {
      this.pokemon = await this.pokemonService.getPokemonData(this.name);
      console.log(this.pokemon);
    } catch (e) {
      console.error(e.message);
    };
  }

  onAddClick() {
    console.log("Click");
    localStorage.setItem('pokemon', JSON.stringify(this.pokemon));
    console.log(JSON.stringify(this.pokemon));
  }
}
